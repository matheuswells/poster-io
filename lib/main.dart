import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:poster_io/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:poster_io/app/models/user_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory =
      await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(UserModelAdapter());
  runApp(ModularApp(module: AppModule()));
}
