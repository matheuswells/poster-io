
class PostModel {
  String content;
  String owner;

  @override
  String toString() {
  return '$content: $owner';
   }

  PostModel({this.content, this.owner});

  factory PostModel.fromJson(Map<String, dynamic> json) {
    return PostModel(content: json['content'], owner: json['owner']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content'] = this.content;
    data['owner'] = this.owner;
    return data;
  }
}
