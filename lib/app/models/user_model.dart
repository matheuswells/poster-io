import 'package:hive/hive.dart';
part 'user_model.g.dart';

@HiveType(typeId: 1)
class UserModel {
  @HiveField(0)
  String name;

  @override
  String toString() {
    return '$name';
  }

  UserModel({this.name});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(name: json['name']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}
