import 'package:flutter_modular/flutter_modular.dart';
import 'package:hasura_connect/hasura_connect.dart';
import 'package:poster_io/app/models/post_model.dart';

class ApiService extends Disposable {
  static final String url = 'https://poster-io.herokuapp.com/v1/graphql';
  static final String key = '##posterio@@';
  final hasuraConnect = HasuraConnect(url);
  Snapshot<PostModel> snapshot;

  String query = """ MySubscription {
  Post {
    content
    owner
  }
}""";

  postSubscription() async {
    

    
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
