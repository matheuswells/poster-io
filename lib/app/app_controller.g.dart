// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppController on _AppBase, Store {
  final _$loggedAtom = Atom(name: '_AppBase.logged');

  @override
  bool get logged {
    _$loggedAtom.context.enforceReadPolicy(_$loggedAtom);
    _$loggedAtom.reportObserved();
    return super.logged;
  }

  @override
  set logged(bool value) {
    _$loggedAtom.context.conditionallyRunInAction(() {
      super.logged = value;
      _$loggedAtom.reportChanged();
    }, _$loggedAtom, name: '${_$loggedAtom.name}_set');
  }

  final _$aAtom = Atom(name: '_AppBase.a');

  @override
  String get a {
    _$aAtom.context.enforceReadPolicy(_$aAtom);
    _$aAtom.reportObserved();
    return super.a;
  }

  @override
  set a(String value) {
    _$aAtom.context.conditionallyRunInAction(() {
      super.a = value;
      _$aAtom.reportChanged();
    }, _$aAtom, name: '${_$aAtom.name}_set');
  }

  final _$_AppBaseActionController = ActionController(name: '_AppBase');

  @override
  dynamic verifyLogin() {
    final _$actionInfo = _$_AppBaseActionController.startAction();
    try {
      return super.verifyLogin();
    } finally {
      _$_AppBaseActionController.endAction(_$actionInfo);
    }
  }
}
