import 'package:flutter_modular/flutter_modular.dart';
import 'package:poster_io/app/modules/create/create_controller.dart';
import 'package:poster_io/app/modules/create/create_page.dart';

class CreateModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => CreateController()),
      ];

  @override
  List<Router> get routers => [
    Router('/create', child: (_, args) => CreatePage()),
  ];

  static Inject get to => Inject<CreateModule>.of();
}
