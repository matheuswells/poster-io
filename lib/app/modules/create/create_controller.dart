import 'package:mobx/mobx.dart';

part 'create_controller.g.dart';

class CreateController = _CreateBase with _$CreateController;

abstract class _CreateBase with Store {
  @observable
  bool btnEnabled = false;
  String content;

  @action
  void checkContent(String value) {
    value.length > 1? btnEnabled = true: btnEnabled = false;
    content = value;
  }

  @action
  void postContent() {
    print(content);
    
  }
}
