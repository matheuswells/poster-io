import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:poster_io/app/modules/create/create_controller.dart';

class CreatePage extends StatefulWidget {
  final String title;
  const CreatePage({Key key, this.title = "Create New Post"}) : super(key: key);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  final createController = CreateController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.fromLTRB(45, 0, 45, 0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 50, 0, 15),
                          child: Text(
                            "Novo Post",
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 25,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Observer(
                            builder: (_) {
                              return TextField(
                                showCursor: true,
                                maxLines: 5,
                                onChanged: createController.checkContent,
                                maxLength: 120,
                                decoration: InputDecoration(
                                    hintText: "Insira uma mensagem",
                                    labelText: "Post"),
                              );
                            },
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Observer(builder: (_) {
                              return FlatButton(
                                  disabledColor: Colors.grey,
                                  color: Colors.blue,
                                  splashColor: Colors.white,
                                  onPressed: createController.btnEnabled
                                      ? () {
                                          //Verificar se nome ja existe
                                          createController.postContent();
                                          Navigator.of(context)
                                              .pushReplacementNamed('/posts');
                                        }
                                      : null,
                                  child: Container(
                                      height: 50,
                                      width: 200,
                                      child: Center(
                                        child: Text(
                                          "Postar",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        ),
                                      )));
                            }))
                      ]))
            ],
          ),
        ],
      ),
    );
  }
}
