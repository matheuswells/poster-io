// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CreateController on _CreateBase, Store {
  final _$btnEnabledAtom = Atom(name: '_CreateBase.btnEnabled');

  @override
  bool get btnEnabled {
    _$btnEnabledAtom.context.enforceReadPolicy(_$btnEnabledAtom);
    _$btnEnabledAtom.reportObserved();
    return super.btnEnabled;
  }

  @override
  set btnEnabled(bool value) {
    _$btnEnabledAtom.context.conditionallyRunInAction(() {
      super.btnEnabled = value;
      _$btnEnabledAtom.reportChanged();
    }, _$btnEnabledAtom, name: '${_$btnEnabledAtom.name}_set');
  }

  final _$_CreateBaseActionController = ActionController(name: '_CreateBase');

  @override
  void checkContent(String value) {
    final _$actionInfo = _$_CreateBaseActionController.startAction();
    try {
      return super.checkContent(value);
    } finally {
      _$_CreateBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void postContent() {
    final _$actionInfo = _$_CreateBaseActionController.startAction();
    try {
      return super.postContent();
    } finally {
      _$_CreateBaseActionController.endAction(_$actionInfo);
    }
  }
}
