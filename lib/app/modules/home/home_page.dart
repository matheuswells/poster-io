import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:hive/hive.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Poster.io"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

@override
void dispose() {
  Hive.box('user').close();
}

class _HomePageState extends State<HomePage> {
  final homeController = HomeController();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Hive.openBox('user'),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState ==
            ConnectionState.done) if (snapshot.error != null)
          return Text(snapshot.error.toString());
        else {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
            ),
            body: ListView(
              children: Hive.box('user').get(0) != null
                  ? <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: FlatButton(
                                  color: Colors.blue,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed('/posts');
                                  },
                                  child: Container(
                                    height: 50,
                                    width: MediaQuery.of(context).size.width/1.2,
                                    child: Center(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            'Continuar como ',
                                            style: TextStyle(color: Colors.white, fontSize: 15),
                                          ),
                                          Text(
                                            '${Hive.box('user').get(0).toString()}',
                                            style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: FlatButton(
                                  color: Colors.blue,
                                  onPressed: () {
                                    Hive.box('user').clear();
                                    setState(() {});//:v dps faço usando mobx
                                  },
                                  child: Container(
                                    height: 50,
                                    width: MediaQuery.of(context).size.width/1.2,
                                    child: Center(
                                      child: Text(
                                        'Sair',
                                        style: TextStyle(color: Colors.white, fontSize: 20),
                                      ),
                                    ),
                                  )),
                            )
                          ],
                        ),
                      )
                    ]
                  : <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(45, 0, 45, 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 50, 0, 15),
                              child: Text(
                                "Bem-vindo ao Poster.io",
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 40,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Text(
                              "Não sei pra que serve mas é legal",
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 20,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: TextField(
                                showCursor: true,
                                onChanged: homeController.checkName,
                                maxLength: 15,
                                decoration: InputDecoration(
                                    hintText: "Insira seu nome",
                                    labelText: "Nome"),
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Observer(builder: (_) {
                                  return FlatButton(
                                      disabledColor: Colors.grey,
                                      color: Colors.blue,
                                      splashColor: Colors.white,
                                      onPressed: homeController.btnEnabled
                                          ? () {
                                              //Verificar se nome ja existe
                                              homeController.saveUser();
                                              Navigator.of(context)
                                                  .pushReplacementNamed(
                                                      '/posts');
                                            }
                                          : null,
                                      child: Container(
                                          height: 50,
                                          width: 200,
                                          child: Center(
                                            child: Text(
                                              "Entrar",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20),
                                            ),
                                          )));
                                })),
                            SizedBox(
                              height: 100,
                            ),
                            Text(
                              "Segue lá: @matheuswells",
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
            ),
          );
        }
        else
          return Scaffold(
            body: Container(
                width: double.infinity,
                color: Colors.blue,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Poster.io",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 60,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          );
      },
    );
  }
}
