import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';
import 'package:poster_io/app/models/user_model.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {

   @observable
   bool btnEnabled = false;
   @observable
   String username;

  @action
  void checkName(String value) {
     value.length > 5 ? btnEnabled = true: btnEnabled = false;
     username = value;
  }

  @action
  saveUser(){
    var newUser = UserModel(name: username);
    final userBox = Hive.box('user');
    userBox.add(newUser);
    print('${newUser.name}');
  }
}
