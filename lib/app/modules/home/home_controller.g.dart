// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeBase, Store {
  final _$btnEnabledAtom = Atom(name: '_HomeBase.btnEnabled');

  @override
  bool get btnEnabled {
    _$btnEnabledAtom.context.enforceReadPolicy(_$btnEnabledAtom);
    _$btnEnabledAtom.reportObserved();
    return super.btnEnabled;
  }

  @override
  set btnEnabled(bool value) {
    _$btnEnabledAtom.context.conditionallyRunInAction(() {
      super.btnEnabled = value;
      _$btnEnabledAtom.reportChanged();
    }, _$btnEnabledAtom, name: '${_$btnEnabledAtom.name}_set');
  }

  final _$usernameAtom = Atom(name: '_HomeBase.username');

  @override
  String get username {
    _$usernameAtom.context.enforceReadPolicy(_$usernameAtom);
    _$usernameAtom.reportObserved();
    return super.username;
  }

  @override
  set username(String value) {
    _$usernameAtom.context.conditionallyRunInAction(() {
      super.username = value;
      _$usernameAtom.reportChanged();
    }, _$usernameAtom, name: '${_$usernameAtom.name}_set');
  }

  final _$_HomeBaseActionController = ActionController(name: '_HomeBase');

  @override
  void checkName(String value) {
    final _$actionInfo = _$_HomeBaseActionController.startAction();
    try {
      return super.checkName(value);
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic saveUser() {
    final _$actionInfo = _$_HomeBaseActionController.startAction();
    try {
      return super.saveUser();
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }
}
