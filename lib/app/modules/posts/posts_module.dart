import 'package:flutter_modular/flutter_modular.dart';
import 'package:poster_io/app/modules/posts/posts_controller.dart';
import 'package:poster_io/app/modules/posts/posts_page.dart';

class PostsModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => PostsController()),
      ];

  @override
  List<Router> get routers => [
        Router('/posts', child: (_, args) => PostsPage()),
      ];

  static Inject get to => Inject<PostsModule>.of();
}
