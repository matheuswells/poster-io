import 'package:flutter/material.dart';
import 'package:poster_io/app/modules/posts/posts_controller.dart';
import 'package:poster_io/app/shared/api_service.dart';

class PostsPage extends StatefulWidget {
  final String title;
  const PostsPage({Key key, this.title = "Posts"}) : super(key: key);

  @override
  _PostsPageState createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  final postsController = PostsController();
  final api = ApiService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pushNamed('/');
              }),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/create');
              },
              child: Row(
                children: <Widget>[
                  Text(
                    'Novo Post',
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(Icons.add, color: Colors.white),
                ],
              ),
            )
          ],
          title: Text(widget.title),
        ),
        body: null);

    /*  ListView.builder(
        itemCount: 5,
        itemBuilder: (context, index) {
          return ListTile(
            dense: false,
            onLongPress: () {},
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "MatheusWells",
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            subtitle: Text(
                "Olá sou o desenvolvedor desse app para testa-lo basta clicar em Novo Post e inserir uma mensagem "),
          );
        },
      ), */
  }
}
